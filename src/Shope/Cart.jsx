import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return  this.props.cart.map((item) => {
              return (
                  <tr>
                   <td>{item.id}</td>
                   <td>{item.name}</td>
                   <td>{item.price * item.soLuong}</td>
                   <td>
                    <img style={{width: 50}} src={item.image} alt="" />
                   </td>
                   <td>
                    <button onClick={() => this.props.chinhSuaSoLuong(item.id, -1)} className='btn btn-danger'>-</button>
                    <strong>{item.soLuong}</strong>
                    <button onClick={() => this.props.chinhSuaSoLuong(item.id, + 1)} className='btn btn-success'>+</button>
                   </td>
                   <td>
                    <button onClick={()=>{ this.props.xoaGioHang(item.id)}} className='btn btn-danger'>
                                Xóa
                    </button>
                   </td>
                  </tr>
              )
          })
       }
    render() {
    return (
      <div>
        <table className='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Img</th>
                    <th>Số lượng</th>
                    <th>Chức Năng</th>
                </tr>
            </thead>
            <tbody>
            {this.renderTbody()}
            </tbody>
        </table>
      </div>
    )
  }
}
