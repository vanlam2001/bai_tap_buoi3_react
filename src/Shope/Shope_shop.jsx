import React, { Component } from 'react'
import { data_shoe } from './data_shoe'
import ListShope from './ListShope'
import Cart from './Cart'

export default class Shope_shop extends Component {
    state = {
        ListShope: data_shoe,
        cart: [],
    }

    chinhSuaSoLuong = (idShoe, luaChon)=>{
      // luaChon: 1 hoặc -1 
      let cloneCart = [...this.state.cart];
       
       let index = cloneCart.findIndex((item) => {
        return item.id == idShoe;
        });
       
        cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;

        this.setState({
          cart: cloneCart,
         })

    }
    handleAd
    
    themGioHang = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = cloneCart.findIndex((item) => {
          return item.id==shoe.id;
          });
          if(index == -1){
               let newShoe={...shoe,soLuong:1};
               
               console.log("🚀 ~ file: Ex_Shoe_Shop.jsx:21 ~ Ex_Shoe_Shop ~ shoe", shoe)
               console.log("🚀 ~ file: Ex_Shoe_Shop.jsx:21 ~ Ex_Shoe_Shop ~ newShoe", newShoe)
               cloneCart.push(newShoe)
          } else{
           cloneCart[index].soLuong++;
          }
          
          this.setState({
           cart: cloneCart,
          })
    }

    xoaGioHang = (idShoe) =>{
     let newCart = this.state.cart.filter((item) =>{
            return (item.id != idShoe);
         });

        this.setState({ cart: newCart });
    }
    



    render() {
    return (
      <div>
        <div className="container">
        <h2>Shope_shop</h2>
          {this.state.cart.length>0&&<Cart chinhSuaSoLuong={this.chinhSuaSoLuong} xoaGioHang={this.xoaGioHang}  cart={this.state.cart}></Cart>}
          <ListShope them={this.themGioHang} list={this.state.ListShope}></ListShope>
        </div>
        </div>
    )
  }
}
